﻿using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public event DragEventHandler OnDrag;

    [SerializeField] private string dragInputName = "Drag";
    [SerializeField] private Transform cursorTransform;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float cursorDistance = 20f;

    private float currentCursorDistance;
    private DragEventArgs.Builder dragEventArgsBuilder = new DragEventArgs.Builder();
    private DragEventArgs lastDragEventArgs;
    private GameObject hitObjectFromCameraRay = null;
	private Vector3 offset;

    private void Start()
    {
        currentCursorDistance = cursorDistance;

        if (!cameraTransform)
            cameraTransform = Camera.main.transform;
    }

    private void Update()
    {
        bool dragInputUp        = Input.GetButtonUp(dragInputName);
        bool dragInputStartDown = Input.GetButtonDown(dragInputName);
        bool dragInputDown      = Input.GetButton(dragInputName);

        if (dragInputUp)
        {
            currentCursorDistance = cursorDistance;

            lastDragEventArgs = dragEventArgsBuilder
                .SetCursorPositionOnStart(lastDragEventArgs.CursorPositionOnStart)
                .SetCurrentCursorPosition(cursorTransform.position)
                .SetLastCursorPosition(lastDragEventArgs.CurrentCursorPosition)
                .SetDragState(DragState.ENDED)
                .SetCurrentGameObjectOnCursor(hitObjectFromCameraRay)
                .SetGameObjectOnCursorOnStart(lastDragEventArgs.GameObjectOnCursorOnStart)
                .SetLastGameObjectOnCursor(lastDragEventArgs.CurrentGameObjectOnCursor)
                .Build();

            lastDragEventArgs.GameObjectOnCursorOnStart?.GetComponentInParent<Draggable>()?.Drag(lastDragEventArgs);
            OnDrag?.Invoke(this, lastDragEventArgs);

        }
        else if (dragInputStartDown)
        {
            lastDragEventArgs = dragEventArgsBuilder
                .SetCursorPositionOnStart(cursorTransform.position)
                .SetCurrentCursorPosition(cursorTransform.position)
                .SetLastCursorPosition(cursorTransform.position)
                .SetDragState(DragState.STARTED)
                .SetCurrentGameObjectOnCursor(hitObjectFromCameraRay)
                .SetGameObjectOnCursorOnStart(hitObjectFromCameraRay)
                .SetLastGameObjectOnCursor(hitObjectFromCameraRay)
                .Build();

            lastDragEventArgs.GameObjectOnCursorOnStart?.GetComponentInParent<Draggable>()?.Drag(lastDragEventArgs);

            OnDrag?.Invoke(this, lastDragEventArgs);
        }
        else if (dragInputDown)
        {
            lastDragEventArgs = dragEventArgsBuilder
                .SetCursorPositionOnStart(lastDragEventArgs.CursorPositionOnStart)
                .SetCurrentCursorPosition(cursorTransform.position)
                .SetLastCursorPosition(lastDragEventArgs.CurrentCursorPosition)
                .SetDragState(DragState.UPDATED)
                .SetCurrentGameObjectOnCursor(hitObjectFromCameraRay)
                .SetGameObjectOnCursorOnStart(lastDragEventArgs.GameObjectOnCursorOnStart)
                .SetLastGameObjectOnCursor(lastDragEventArgs.CurrentGameObjectOnCursor)
                .Build();

            lastDragEventArgs.GameObjectOnCursorOnStart?.GetComponentInParent<Draggable>()?.Drag(lastDragEventArgs);

            OnDrag?.Invoke(this, lastDragEventArgs);
			if (hitObjectFromCameraRay.tag == "MoveHandler")
			{
				GameObject par = hitObjectFromCameraRay.transform.parent.gameObject;
				offset =  offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
				par.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + offset;
			}
			if (hitObjectFromCameraRay.tag == "ScaleHandler") 
			{
				GameObject par = hitObjectFromCameraRay.transform.parent.gameObject.transform.parent.gameObject.transform.GetChild (4).gameObject;
				if (Input.GetKeyDown("j"))
					par.transform.localScale += new Vector3 (0.1f, 0.1f, 0.1f);
				if (Input.GetKeyDown ("k")) 
					par.transform.localScale -= new Vector3 (0.1f, 0.1f, 0.1f);
			}
			if (hitObjectFromCameraRay.tag == "RotationHandler") 
			{
				GameObject par = hitObjectFromCameraRay.transform.parent.gameObject.transform.parent.gameObject.transform.GetChild (4).gameObject;
				if (Input.GetKeyDown ("j")) 
				{
					if (hitObjectFromCameraRay.transform.localRotation.x == 0)
						par.transform.Rotate(Vector3.right * Time.deltaTime);
					if (hitObjectFromCameraRay.transform.localRotation.x == 90)
						par.transform.Rotate(Vector3.up * Time.deltaTime);
					if (hitObjectFromCameraRay.transform.localRotation.x == 180)
						par.transform.Rotate(Vector3.forward * Time.deltaTime);
				}
				if (Input.GetKeyDown ("k")) 
					par.transform.localScale -= new Vector3 (0.1f, 0.1f, 0.1f);
			}
        }

        RaycastHit hitInfo;
        Ray cameraRay = new Ray(cameraTransform.position, cameraTransform.forward);

        if (Physics.Raycast(cameraRay, out hitInfo, cursorDistance))
        {
            if (!dragInputDown)
            {
                cursorTransform.position = hitInfo.point;
                currentCursorDistance = hitInfo.distance;
            }

            hitObjectFromCameraRay = hitInfo.collider.gameObject;
        }
        else
        {
            cursorTransform.position = cameraRay.GetPoint((dragInputDown) ? currentCursorDistance : cursorDistance);
            hitObjectFromCameraRay = null;
        }
    }
}
