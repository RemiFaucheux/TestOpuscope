﻿using UnityEngine;

public delegate void DragEventHandler(object sender, DragEventArgs eventArgs);

public class DragEventArgs
{
    public DragState DragState { get; private set; }
    public Vector3 CursorPositionOnStart { get; private set; }
    public Vector3 LastCursorPosition { get; private set; }
    public Vector3 CurrentCursorPosition { get; private set; }

    public GameObject GameObjectOnCursorOnStart { get; private set; }
    public GameObject LastGameObjectOnCursor { get; private set; }
    public GameObject CurrentGameObjectOnCursor { get; private set; }

    public Vector3 CursorTranslation => CurrentCursorPosition - LastCursorPosition;

    private DragEventArgs()
    {
    }

    public class Builder
    {
        private Vector3 cursorPositionOnStart;
        private Vector3 lastCursorPosition;
        private Vector3 currentCursorPosition;

        private DragState dragState;

        private GameObject gameObjectOnCursorOnStart;
        private GameObject lastGameObjectOnCursor;
        private GameObject currentGameObjectOnCursor;

        public Builder Reset()
        {
            cursorPositionOnStart     = default(Vector3);
            lastCursorPosition        = default(Vector3);
            currentCursorPosition     = default(Vector3);
            gameObjectOnCursorOnStart = null;
            lastGameObjectOnCursor    = null;
            currentGameObjectOnCursor = null;

            return this;
        }

        public Builder SetDragState(DragState dragState)
        {
            this.dragState = dragState;
            return this;
        }

        public Builder SetGameObjectOnCursorOnStart(GameObject gameObjectOnCursorOnStart)
        {
            this.gameObjectOnCursorOnStart = gameObjectOnCursorOnStart;
            return this;
        }

        public Builder SetLastGameObjectOnCursor(GameObject lastGameObjectOnCursor)
        {
            this.lastGameObjectOnCursor = lastGameObjectOnCursor;
            return this;
        }

        public Builder SetCurrentGameObjectOnCursor(GameObject currentGameObjectOnCursor)
        {
            this.currentGameObjectOnCursor = currentGameObjectOnCursor;
            return this;
        }

        public Builder SetCursorPositionOnStart(Vector3 cursorPositionOnStart)
        {
            this.cursorPositionOnStart = cursorPositionOnStart;
            return this;
        }

        public Builder SetLastCursorPosition(Vector3 lastCursorPosition)
        {
            this.lastCursorPosition = lastCursorPosition;
            return this;
        }

        public Builder SetCurrentCursorPosition(Vector3 currentCursorPosition)
        {
            this.currentCursorPosition = currentCursorPosition;
            return this;
        }

        public DragEventArgs Build()
        {
            DragEventArgs downEvent             = new DragEventArgs();
            downEvent.CurrentCursorPosition     = currentCursorPosition;
            downEvent.CurrentGameObjectOnCursor = currentGameObjectOnCursor;
            downEvent.CursorPositionOnStart     = cursorPositionOnStart;
            downEvent.GameObjectOnCursorOnStart = gameObjectOnCursorOnStart;
            downEvent.LastCursorPosition        = lastCursorPosition;
            downEvent.LastGameObjectOnCursor    = lastGameObjectOnCursor;
            downEvent.DragState                 = dragState;

            return downEvent;
        }
    }
}