using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [Serializable]
    public class MouseLook
    {
        [SerializeField] private float xSensitivity = 2f;
        [SerializeField] private float ySensitivity = 2f;
        [SerializeField] private bool clampVerticalRotation = true;
        [SerializeField] private float minimumX = -90F;
        [SerializeField] private float maximumX = 90F;
        [SerializeField] private bool smooth;
        [SerializeField] private float smoothTime = 5f;
        [SerializeField] private bool _lockCursor = true;

        public bool LockCursor
        {
            get { return _lockCursor; }
            set {
                _lockCursor = value;

                if (!value)
                {
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                }
            }
        }

        private Quaternion characterTargetRot;
        private Quaternion cameraTargetRot;
        private bool cursorIsLocked = true;

        public void Init(Transform character, Transform camera)
        {
            characterTargetRot = character.localRotation;
            cameraTargetRot    = camera.localRotation;
        }

        public void LookRotation(Transform character, Transform camera)
        {
            float yRot = Input.GetAxis("Mouse X") * xSensitivity;
            float xRot = Input.GetAxis("Mouse Y") * ySensitivity;

            characterTargetRot *= Quaternion.Euler (0f, yRot, 0f);
            cameraTargetRot    *= Quaternion.Euler (-xRot, 0f, 0f);

            if(clampVerticalRotation)
                cameraTargetRot = ClampRotationAroundXAxis (cameraTargetRot);

            if(smooth)
            {
                character.localRotation = Quaternion.Slerp (character.localRotation, characterTargetRot, smoothTime * Time.deltaTime);
                camera.localRotation    = Quaternion.Slerp (camera.localRotation, cameraTargetRot, smoothTime * Time.deltaTime);
            }
            else
            {
                character.localRotation = characterTargetRot;
                camera.localRotation    = cameraTargetRot;
            }

            UpdateCursorLock();
        }

        public void UpdateCursorLock()
        {
            if (!_lockCursor)
                return;

            if (Input.GetKeyUp(KeyCode.Escape))
                cursorIsLocked = false;
            else if(Input.GetMouseButtonUp(0))
                cursorIsLocked = true;

            Cursor.lockState = (cursorIsLocked) ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible   = !cursorIsLocked;
        }

        private Quaternion ClampRotationAroundXAxis(Quaternion rotation)
        {
            rotation.x /= rotation.w;
            rotation.y /= rotation.w;
            rotation.z /= rotation.w;
            rotation.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (rotation.x);

            angleX = Mathf.Clamp (angleX, minimumX, maximumX);

            rotation.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);

            return rotation;
        }
    }
}
