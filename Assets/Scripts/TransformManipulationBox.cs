﻿using UnityEngine;

public class TransformManipulationBox : MonoBehaviour {
    [SerializeField] private Transform encompassedTransform;

	public GameObject obj;

	private float x;
	private float y;
	private float z;

	void Start()
	{
		obj = this.gameObject.transform.GetChild (4).gameObject;
	}

	void Update()
	{
		x = obj.GetComponent<MeshFilter> ().mesh.bounds.size.x * obj.GetComponent<Transform>().localScale.x;
		y = obj.GetComponent<MeshFilter> ().mesh.bounds.size.y * obj.GetComponent<Transform>().localScale.y;
		z = obj.GetComponent<MeshFilter> ().mesh.bounds.size.z * obj.GetComponent<Transform>().localScale.z;

		move (-x / 2, -y / 2, -z / 2, 0, 0);
		move (-x / 2, -y / 2, z / 2, 0, 1);
		move (x / 2, -y / 2, -z / 2, 0, 2);
		move (x / 2, -y / 2, z / 2, 0, 3);
		move (-x / 2, y / 2, -z / 2, 0, 4);
		move (-x / 2, y / 2, z / 2, 0, 5);
		move (x / 2, y / 2, -z / 2, 0, 6);
		move (x / 2, y / 2, z / 2, 0, 7);

		move (x / 2, 0f, z / 2, 1, 0);
		move (x / 2, 0f, -z / 2, 1, 1);
		move (-x / 2, 0f, z / 2, 1, 2);
		move (-x / 2, 0f, -z / 2, 1, 3);
		move (0f, y / 2, z / 2, 1, 4);
		move (0f, y / 2, -z / 2, 1, 5);
		move (x / 2, y / 2, 0f, 1, 6);
		move (-x / 2, y / 2, 0f, 1, 7);
		move (0f, -y / 2, z/2, 1, 8);
		move (0f, -y / 2, -z / 2, 1, 9);
		move (x / 2, -y / 2, 0f, 1, 10);
		move (-x / 2, -y / 2, 0f, 1, 11);

		move (x / 2, 0f, z / 2, 2, 0);
		move (x / 2, 0f, -z / 2, 2, 1);
		move (-x / 2, 0f, z / 2, 2, 2);
		move (-x / 2, 0f, -z / 2, 2, 3);
		move (0f, y / 2, z / 2, 2, 4);
		move (0f, y / 2, -z / 2, 2, 5);
		move (x / 2, y / 2, 0f, 2, 6);
		move (-x / 2, y / 2, 0f, 2, 7);
		move (0f, -y / 2, z/2, 2, 8);
		move (0f, -y / 2, -z / 2, 2, 9);
		move (x / 2, -y / 2, 0f, 2, 10);
		move (-x / 2, -y / 2, 0f, 2, 11);

		rescaleY (2, 0);
		rescaleY (2, 1);
		rescaleY (2, 2);
		rescaleY (2, 3);
		rescaleX (2, 4);
		rescaleX (2, 5);
		rescaleZ (2, 6);
		rescaleZ (2, 7);
		rescaleX (2, 8);
		rescaleX (2, 9);
		rescaleZ (2, 10);
		rescaleZ (2, 11);
		rescale ();
	}

	void move(float x, float y, float z, int g1, int g2)
	{
		GameObject obj = this.gameObject.transform.GetChild (g1).GetChild (g2).gameObject;
		Vector3 tmp = new Vector3 (this.transform.position.x + x, this.transform.position.y + y, this.transform.position.z + z);
		obj.transform.position = tmp;
	}

	void rescaleZ(int g1, int g2)
	{
		GameObject obj = this.gameObject.transform.GetChild (g1).GetChild (g2).gameObject;
		float size = obj.GetComponent<MeshFilter> ().mesh.bounds.size.z;
		Vector3 res = obj.transform.localScale;
		res.z = z / size;
		obj.transform.localScale = res;
	}

	void rescaleY(int g1, int g2)
	{
		GameObject obj = this.gameObject.transform.GetChild (g1).GetChild (g2).gameObject;
		float size = obj.GetComponent<MeshFilter> ().mesh.bounds.size.y;
		Vector3 res = obj.transform.localScale;
		res.y = y / size;
		obj.transform.localScale = res;
	}

	void rescaleX(int g1, int g2)
	{
		GameObject obj = this.gameObject.transform.GetChild (g1).GetChild (g2).gameObject;
		float size = obj.GetComponent<MeshFilter> ().mesh.bounds.size.x;
		Vector3 res = obj.transform.localScale;
		res.x = x / size;
		obj.transform.localScale = res;
	}

	void rescale()
	{
		GameObject obj = this.gameObject.transform.GetChild (3).gameObject;
		float sizeX = obj.GetComponent<MeshFilter> ().mesh.bounds.size.x;
		float sizeY = obj.GetComponent<MeshFilter> ().mesh.bounds.size.y;
		float sizeZ = obj.GetComponent<MeshFilter> ().mesh.bounds.size.z;
		Vector3 res = obj.transform.localScale;
		res.x = x / sizeX;
		res.y = y / sizeY;
		res.z = z / sizeZ;
		obj.transform.localScale = res;
	}

}
